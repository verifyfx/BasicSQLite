package com.egco428.basicsqlite;

import android.app.ListActivity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;

import java.sql.Struct;
import java.util.List;
import java.util.Random;

public class MainActivity extends ListActivity {

    private CommentsDataSource dataSource;
    private EditText txt1;
    private EditText txt2;// = (EditText)findViewById(R.id.editText2)

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txt1 = (EditText)findViewById(R.id.editText);
        txt2 = (EditText)findViewById(R.id.editText2);

        dataSource = new CommentsDataSource(this);
        dataSource.open();

        List<Comment> values = dataSource.getAllComments();
        ArrayAdapter<Comment> adapter = new ArrayAdapter<Comment>(this, android.R.layout.simple_list_item_1, values);
        setListAdapter(adapter);
    }

    public void onClick(View view) {
        ArrayAdapter<Comment> adapter = (ArrayAdapter<Comment>) getListAdapter();
        Comment comment = null;
        switch (view.getId()) {
            case R.id.addBtn:
                //String [] comments = new String [] {"Wow", "Such comment", "Much complex", "Very confuse"};
                //int nextInt = new Random().nextInt(4);
                //comment = dataSource.createComment(comments[nextInt]);
                comment = dataSource.createComment(String.valueOf(txt1.getText())+" "+ String.valueOf(txt2.getText()));
                adapter.add(comment);
                txt1.setText("");
                txt2.setText("");
                break;
            case R.id.deleteBtn:
                if(getListAdapter().getCount()>0) {
                    comment = (Comment) getListAdapter().getItem(0);
                    dataSource.deleteComment(comment);
                    adapter.remove(comment);
                }
                break;
            default:
                break;
        }
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onResume() {
        dataSource.open();
        super.onResume();
    }

    @Override
    public void onPause() {
        dataSource.close();
        super.onPause();
    }
}
